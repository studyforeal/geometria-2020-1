Narrativa:
Como um Usuario
desejo efetuar calculos geometricos em triagulos 
de modo que possa saber o valor da hipotenusa ou cateto

Cenário: Calculo da hipotenusa
Dado que estou na funcionalidade de cálculo de triângulos
Quando seleciono o tipo de cálculo Hipotenusa
E informo 3 para cateto1 
E informo 4 para cateto2
E solicito que o cálculo seja realizado 
Então a hipotenusa calculada será 5

Cenário: Calculo do cateto
Dado que estou na funcionalidade de cálculo de triângulos
Quando seleciono o tipo de cálculo Cateto
E informo 6 para cateto1 
E informo 10 para hipotenusa
E solicito que o cálculo seja realizado 
Então o cateto2 calculado será 8

