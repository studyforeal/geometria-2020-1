package geometria;

import static org.junit.Assert.assertEquals;


import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GeometriaSteps {
	private WebDriver driver;
	
	public GeometriaSteps(WebDriver driver) {
		this.driver = driver;
	}
	
	@AfterClass
	public void fecharDriver() {
		driver.quit();
	}


	@Given("estou na funcionalidade de cálculo de triângulos")
	public void goToPage() {
		driver.get(System.getProperty("user.dir") +"\\src\\main\\webapp\\triangulo.html");
	}

	@When("seleciono o tipo de cálculo $campo")
	public void selecionarSelect(String campo) throws InterruptedException {
		driver.findElement(By.id("tipoCalculoSelect")).click();
		Thread.sleep(500);
		driver.findElement(By.id("tipoCalculoSelect")).sendKeys(campo);
		Thread.sleep(500);
		driver.findElement(By.id("tipoCalculoSelect")).sendKeys(Keys.ENTER);
	}

	@When("informo $valor para $idcampo")
	public void preencheInput(String valor,String idcampo) throws InterruptedException {
		driver.findElement(By.id(idcampo)).clear();
		Thread.sleep(500);
		driver.findElement(By.id(idcampo)).sendKeys(valor);
		Thread.sleep(500);
	}

	@When("solicito que o cálculo seja realizado")
	public void clicarCalclar() throws InterruptedException {
		driver.findElement(By.id("calcularBtn")).click();
		Thread.sleep(500);
	}

	@Then("o $idcampo calculado será $valor")
	public void resultadoEsperado(String idcampo,String valor) {
		assertEquals(driver.findElement(By.id(idcampo)).getAttribute("value"),valor);
	}


}
