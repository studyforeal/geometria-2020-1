package geometria;

import org.junit.BeforeClass;
import org.openqa.selenium.firefox.FirefoxDriver;

public class withFirefox extends GeometriaTest {
	
	@BeforeClass
	public static void withFirefox() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
		driver = new FirefoxDriver();
	}
}
