package geometria;


import org.junit.BeforeClass;
import org.openqa.selenium.chrome.ChromeDriver;

public class withChrome extends GeometriaTest{
	
	@BeforeClass
	public static void withChrome() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}
	
}
